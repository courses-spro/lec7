const Router = require("../util/router");
const {handleUserGet, handleUserDelete, handleUserOptions, handleUserPost} = require('../controller/user');

function subscribeUserRouter(server) {
  const router = new Router(server, "/user");

  router.get("", handleUserGet);

  router.post("", handleUserPost);

  router.options("", handleUserOptions);

  router.delete("", handleUserDelete);
}
module.exports = {
  subscribeUserRouter,
};
