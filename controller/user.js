
const userService = require('../service/user');

function handleUserGet(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.statusCode = 200;
  res.end(JSON.stringify(userService.getUserList()));
}

function handleUserPost(req, res) {
  res.setHeader("Content-Type", "application/json");

  res.end(userService.createUser(req.body));
}

function handleUserOptions(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.end();
}

function handleUserDelete(req, res) {
  res.setHeader("Content-Type", "application/json");
  userService.deleteUser(req.body);
  res.end(JSON.stringify(userService.getUserList()));
}

module.exports = {
  handleUserGet,
  handleUserPost,
  handleUserOptions,
  handleUserDelete
};
