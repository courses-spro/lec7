
const userList = [
  { id: 0, name: "Dima", age: 25 },
  { id: 1, name: "Vasya", age: 17 },
  { id: 2, name: "Petya", age: 213 },
  { id: 3, name: "Sasha", age: 32 },
  { id: 4, name: "Kolya", age: 54 },
  { id: 5, name: "Igor", age: 56 },
  { id: 6, name: "Ura", age: 23 },
  { id: 7, name: "Vasya", age: 45 },
];

function getUserList() {
  return userList;
}

function createUser(payload) {
  const newUser = JSON.parse(payload);

  userList.push(newUser);
  return newUser;
}

function deleteUser(userId) {
  const position = userList.map((user) => user.id).indexOf(userId);
  if (position > -1) userList.splice(position, 1);
}


module.exports = {
  getUserList,
  createUser,
  deleteUser
};
